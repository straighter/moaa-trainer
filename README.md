# MOAA trainer

This CNN trainer script has been created as part of a *prototype* for a graduation assignment.

Inspired by: https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html and https://deeplearningsandbox.com/how-to-use-transfer-learning-and-fine-tuning-in-keras-and-tensorflow-to-build-an-image-recognition-94b0b02444f2