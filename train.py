import argparse
import os
import datetime
import glob
import pickle
import numpy
import plaidml.keras

# Use PlaidML backend over Tf
plaidml.keras.install_backend()

# Set random seed in order to get reproducible results
numpy.random.seed(28)

from keras.callbacks import Callback
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.applications.inception_v3 import InceptionV3
from keras.models import Model, load_model
from keras.layers import Dense, GlobalAveragePooling2D, Dropout
from keras import backend as K
from keras.optimizers import SGD
import matplotlib.pyplot as plt


# Custom history callback to use for keeping track of training history per batch & epoch
class BatchHistory(Callback):
    def on_train_begin(self, logs={}):
        self.epoch_loss = []
        self.epoch_val_loss = []
        self.epoch_acc = []
        self.epoch_val_acc = []
        self.batch_loss = []
        self.batch_acc = []

    def on_batch_end(self, batch, logs={}):
        self.batch_loss.append(logs.get('loss'))
        self.batch_acc.append(logs.get('acc'))
    
    def on_epoch_end(self, epoch, logs={}):
        self.epoch_loss.append(logs.get('loss'))
        self.epoch_acc.append(logs.get('acc'))
        self.epoch_val_loss.append(logs.get('val_loss'))
        self.epoch_val_acc.append(logs.get('val_acc'))


# Main train function
def train(data_path, save_path, base_model_path, epochs, batch_size):
    # Set variables
    if save_path is None: save_path = './'
    if epochs is None: epochs = 6
    if batch_size is None: batch_size = 16

    num_classes = get_num_items_in_dir(os.path.join(data_path, 'train'))
    num_train = get_num_items_in_dirs(os.path.join(data_path, 'train'))
    num_val = get_num_items_in_dirs(os.path.join(data_path, 'val'))
    classes = get_dir_names(os.path.join(data_path, 'train'))
    train_generator, val_generator = create_generators(data_path, batch_size)
    
    base_model = create_base_model(base_model_path)
    model = replace_classification_layer(base_model, num_classes)

    # Transfer learning
    setup_transfer_learning(base_model, model)
    tl_history = BatchHistory()

    model.fit_generator(
        train_generator,
        epochs=epochs,
        steps_per_epoch=num_train // batch_size,
        validation_data=val_generator,
        validation_steps=num_val // batch_size,
        class_weight='auto',
        callbacks=[tl_history]
    )
    save_history(tl_history, os.path.join(save_path, 'tl_history'))

    # Fine tune
    setup_finetuning(model)
    ft_history = BatchHistory()

    fine_tune_logs = model.fit_generator(
        train_generator,
        epochs=epochs,
        steps_per_epoch=num_train // batch_size,
        validation_data=val_generator,
        validation_steps=num_val // batch_size,
        class_weight='auto',
        callbacks=[ft_history]
    )


    save_history(ft_history, os.path.join(save_path, 'ft_history'))

    model.save(os.path.join(save_path, 'model.hd5'))

# Save history to disk after training session
def save_history(history, path):
    history = {
        'epoch_loss': history.epoch_loss,
        'epoch_val_loss': history.epoch_val_loss,
        'epoch_acc': history.epoch_acc,
        'epoch_val_acc': history.epoch_val_acc,
        'batch_acc': history.batch_acc,
        'batch_loss': history.batch_loss,
    }

    with open(path + '.txt', 'wb') as handle:
        pickle.dump(history, handle)

# Create base model based on Inception if not using a custom base model
def create_base_model(base_model_path):
    if base_model_path and os.path.exists(base_model_path):
        return load_model(base_model_path)

    return InceptionV3(weights='imagenet', include_top=False)

# Replace last network layer (binary style) for own needs
def replace_classification_layer(base_model, num_classes):
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x) 

    # Replace 1 with num_classes in order and sigmoid with softmax for multiclass classification
    classification_layer = Dense(1, activation='sigmoid')(x) 
    model = Model(input=base_model.input, output=classification_layer)
    return model

# Setup the network for transfer learning
def setup_transfer_learning(base_model, model):
    # Freeze all layers and compile the model
    for layer in base_model.layers:
        layer.trainable = False

    # Replace with categorical_crossentropy for multiclass classification
    model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['acc'])

# Setup for finetuning
def setup_finetuning(model):
    # Freeze all except last few layers
    for layer in model.layers[:249]:
        layer.trainable = False
    for layer in model.layers[249:]:
        layer.trainable = True    
    
    # Replace with categorical_crossentropy for multiclass classification
    model.compile(optimizer=SGD(lr=0.0001, momentum=0.9), loss='binary_crossentropy', metrics=['acc'])

# Get label names
def get_dir_names(directory):
    if not os.path.exists(directory):
        return None

    return [dir for dir in os.listdir()]

# Get number of items in all directories
def get_num_items_in_dirs(directory):
    if not os.path.exists(directory):
        return None

    num = 0

    for root, subdirs, files in os.walk(directory):
        for dir in subdirs:
            num += get_num_items_in_dir(os.path.join(root, dir))
    
    return num

# Get number of items in one directory
def get_num_items_in_dir(directory):
    if not os.path.exists(directory):
        return 0

    num = 0

    for dir in os.listdir(directory):
        num += 1

    return num

# Create keras training generators with preprocessing
def create_generators(data_path, batch_size):
    train_datagen = ImageDataGenerator(
        width_shift_range=0.2,
        height_shift_range=0.2,
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest'
    )

    val_datagen = ImageDataGenerator(
        rescale=1./255
    )

    train_generator = train_datagen.flow_from_directory(
        os.path.join(data_path, 'train'),
        target_size=(299, 299),
        batch_size=batch_size,
        class_mode='binary',
    )

    val_generator = val_datagen.flow_from_directory(
        os.path.join(data_path, 'val'),
        target_size=(299, 299),
        batch_size=batch_size,
        class_mode='binary'
    )

    # Print to know which prediction number corresponds to which label
    print(train_generator.class_indices)

    return (train_generator, val_generator)

# Main entry & args handling
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train a model')
    parser.add_argument('dataPath', metavar='dataPath', type=str, help='A path to the data to train.')
    parser.add_argument('--savePath', type=str, help='A save path for models and history')
    parser.add_argument('--baseModelPath', type=str, help='A path for the model to use as a base model')
    parser.add_argument('--epochs', type=int, help='Amount of epochs to learn for')
    parser.add_argument('--batchSize', type=int, help='Size of the batches per train iteration')

    args = parser.parse_args()

    train(args.dataPath, args.savePath, args.baseModelPath, args.epochs, args.batchSize)

